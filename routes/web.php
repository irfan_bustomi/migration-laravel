<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
Route::get('/home', 'RegisterController@home');
Route::get('/register', 'RegisterController@register');
Route::get('/welcome', 'RegisterController@welcome');

*/


route::get('/master', function(){
    return view('adminLTE.master');
});

route::get('/', function(){
    return view('items.index');
});

route::get('/data-tables', function(){
    return view('items.data-tables');
});




